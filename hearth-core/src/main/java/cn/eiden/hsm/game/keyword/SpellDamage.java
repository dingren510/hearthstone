package cn.eiden.hsm.game.keyword;

/**
 * @author Eiden J.P Zhou
 * @date 2020/3/19 16:14
 */
public interface SpellDamage {
    int spellDamageAdd();
}
